set terminal svg size 16384,256 lw 1
set output 'diseqc_wave.svg'

#set terminal pngcairo size 16384,512 lw 1
#set output 'diseqc_wave.png'

plot 'diseqc_wave.txt' u 1:2 w lines

