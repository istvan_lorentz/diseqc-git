

.PHONY: all test svg i

all: diseqc

diseqc: diseqc.cpp
	g++ -o diseqc -O3 diseqc.cpp -lasound 

test: svg

svg:
	./diseqc
	gnuplot diseqc.t

i:
	indent -linux diseqc.cpp

clean:
	rm -f diseqc
	rm -f diseqc_wave.svg
	rm -f diseqc_wave.txt



