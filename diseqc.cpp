/*

 Generate DiSEqC command using sound card (ALSA interface)
 
 
 References
 Diseqc bus specs


 http://mstl.atl.calpoly.edu/~bklofas/Presentations/AMSAT-NA_Symposium2013/Bern_Montgomery_College_Rotator.pdf

*/

/* Use the newer ALSA API */
#define ALSA_PCM_NEW_HW_PARAMS_API
#define _USE_MATH_DEFINES

#include <cmath>
#include <alsa/asoundlib.h>
#include <assert.h>
#include <stdio.h>
#include <vector>
#include <string>
#include <list>
#include <map>
#include <stdint.h>
#include <unistd.h>
#include <getopt.h>


typedef std::vector<uint8_t> ByteBuffer;

struct AudioBuffer {
	std::vector<short> samples;	// channel samples interleaved
	double rate,t,dt,amp;

	// DiSEqC carrier frequency, 22000hz
	static int diseqc_freq;

	unsigned int channels;
	unsigned long frames;
	snd_pcm_t *handle;

	FILE *gpf; // text file, for gnuplot

	AudioBuffer(snd_pcm_t *_handle, snd_pcm_hw_params_t *params) :
			handle(_handle), samples(), rate(0), channels(0), frames(0),gpf(NULL)
	{

		unsigned int rate_num = 0, rate_den = 0, val = 0;
		int dir = 0;
		snd_pcm_hw_params_get_rate_numden(params, &rate_num, &rate_den);
		rate = (double) rate_num / rate_den;
		printf("rate = %d/%d = %g Hz;  %g samples/tick\n", rate_num, rate_den,
				rate,rate/diseqc_freq
				);
		printf("tick duration=%g us\n",tick()*1E6);

		snd_pcm_hw_params_get_channels(params, &channels);
		printf("channels = %d\n", channels);

		snd_pcm_hw_params_get_rate_resample(handle, params, &val);
		printf("Resampling: %d\n", val);

		/* Use a buffer large enough to hold one period */
		snd_pcm_hw_params_get_period_size(params, &frames, &dir);
		snd_pcm_hw_params_get_period_time(params, &val, &dir);
		printf("Frame=%lu samples; %d us\n", frames,val);

		assert(channels >= 1 && channels <= 2);
		assert(frames >= 1);

		t = 0;
		dt = 2.0 * M_PI * (double)diseqc_freq / rate;
		amp=1;
		if (false)
		{
			gpf = fopen("diseqc_wave.txt", "w+b");
		}

	}

	~AudioBuffer() {
		snd_pcm_drain(handle);
		snd_pcm_close(handle);
		handle = NULL;
		if (gpf) {
			fclose(gpf);
			gpf = NULL;
		}
	}
	/* we define a tick as the period of the DiSEqC carrier frequency (22Khz)
	 * a bit is sent in 33 ticks
	 * */
	static double tick()
	{
		return 1.0/diseqc_freq;
	}
	// convert milliseconds to 22Khz-period 'ticks'
	static int msToTicks(double ms)
	{
		return ms/1000.0/tick();
	}
	// convert ticks to samples
	int ticksToSamples(int n) const
	{
		return n * rate / diseqc_freq;
	}
	// returns the recommended frame size
	static int recommendedFrame(double rate)
		{
			// 3 * 9-bit time
			return 10*9.0*33.0*rate/diseqc_freq;
		}

	// emit an audio sample (in sampling frequency rate)
	void emitSample(short v) {
		samples.push_back(v);
		if (channels == 2)
			samples.push_back(v);
		dumpSample(v);
		if (samples.size() >= frames) {
			flush();
		}

	}

	// generate a tone for n ticks
	void tone(int n) {
		n = ticksToSamples(n);
		double A = 32767*amp; // increasing amp>1
		for (int i = 0; i < n; ++i) {
			double y = std::min(32767.0, std::max(-32768.0, A * sin(t)));
			short v = (short) y;
			emitSample(v);
			t+=dt;
		}
	}

	// generate silence for n ticks
	void silence(int n) {
		n = ticksToSamples(n);
		short v = (short) 0;
		for (int i = 0; i < n; ++i) {
			emitSample(v);
		}
	}

	// emits a '0' bit
	void zero() {
		tone(22);
		silence(11);
	}
	// emits an '1' bit
	void one() {
		tone(11);
		silence(22);
	}
	// emits a byte (8 bits, msb first) and an odd parity bit
	void emitByte(uint8_t b) {
		bool p = false;
		printf("0x%02x ",(unsigned int)b);fflush(stdout);
		for (int i = 7; i >= 0; --i) {
			if ((b >> i) & 1) {
				one();
				p = !p;
			} else
				zero();

		}
		if (p)
			zero();
		else
			one();
	}
	// a standard pause is for 10 bits-time
	void pause() {
		silence(33*10);
		printf("\n");
	}

	// emits a message (a sequence of 3..6 bytes, followed by a pause)
	void emitMessage(const uint8_t * buf, size_t numBytes) {
		pause();
		for (size_t i = 0; i < numBytes; ++i) {
			emitByte(buf[i]);
		}
		pause();
	}
	// print the waveform in gnuplot format
	void dumpSample(short v) {
		static int i = 0;
		if (gpf) {
			double x = i / rate;
			double y = v / 32768.0;
			fprintf(gpf, "%16.6f %16.6f\n", x, y);

		}
		i++;
	}

	// returns the number of samples currently accumulated
	int getNumSamples() const {
		return samples.size() / channels;
	}
	// flush the internal buffer towards the soundcard
	void flush() {
		if (getNumSamples() > 0) {
			int rc = snd_pcm_writei(handle, samples.data(), getNumSamples());

			if (rc == -EPIPE) {
				/* EPIPE means underrun */
				fprintf(stderr, "underrun occurred\n");
				snd_pcm_prepare(handle);
			} else if (rc < 0) {
				fprintf(stderr, "error from writei: %s\n", snd_strerror(rc));
			} else if (rc != getNumSamples()) {
				fprintf(stderr, "short write, write %d frames\n", rc);
			}
		}
		samples.clear();

	}
};

#pragma pack(push,1)
struct DiseqcMessage {
	uint8_t framing, address, command;
	uint8_t data[3];

	size_t dataLen;

	DiseqcMessage() :
			framing(FIRST_FROM_MASTER_NO_REPLY), // command from master, no reply required,
			address(POLAR_AZIMUTH_POSITIONER),
			command(DRIVE_EAST), dataLen(1) {
		memset(data, 0, sizeof(data));
		data[0] =  0xf0;
	}

	const uint8_t *ptr() const {
		return &framing;
	}
	size_t numBytes() const {
		return 3 + dataLen;
	}

	enum Framing {
		FIRST_FROM_MASTER_NO_REPLY = 0xE0,//
		REPEAT_FROM_MASTER_NO_REPLY = 0xE1,

	};
	enum Address {
		ANY_POSITIONER = 0x30,
		POLAR_AZIMUTH_POSITIONER=0x31,
		ELEVATION_POSITIONER=0x32
	};
	enum Command {
		RESET = 0,
		HALT       = 0x60,
		// drive commands, data is
		// 0: drive continuously
		// positive 01 .. 0x7f drive in seconds
		// negative 0x80 .. 0xff number of steps
		DRIVE_EAST = 0x68,
		DRIVE_WEST = 0x69,
		GOTO       = 0x6B,
		GOTO_A     = 0x6E,
	};

};
#pragma pack(pop)


typedef std::list<DiseqcMessage> DiseqcMessageList;

typedef std::map<std::string, uint32_t> StringValueMap;

// the standard DiSEqC frequency is 22000 Hz, can be altered by command line -f option
int AudioBuffer::diseqc_freq = 22000;


/** parse a string list and build a Diseqc message list */

void BuildDiseqcMessageList(int argc, char**argv, int optind, DiseqcMessageList & lst)
{
	DiseqcMessage msg;
	msg.framing = DiseqcMessage::FIRST_FROM_MASTER_NO_REPLY;
	while(optind < argc){
				const char * c = argv[optind++]; // command
				int nd=0; // number of data bytes
				switch(c[0])
				{
					case 'G': msg.command=DiseqcMessage::GOTO;nd=1;break;
					case 'g': msg.command=DiseqcMessage::GOTO_A;nd=2;break;
					case 'h': msg.command=DiseqcMessage::HALT;nd=0;break;
					case 'r': msg.command=DiseqcMessage::RESET;nd=0;break;
					case 'e': msg.command=DiseqcMessage::DRIVE_EAST;nd=1;break;
					case 'w': msg.command=DiseqcMessage::DRIVE_WEST;nd=1;break;
					default:
						fprintf(stderr,"Unknown DiSeqC message %s\n",c);
						exit(1);
				}
				if (nd<0 || nd>3)
				{
					fprintf(stderr,"Invalid data length\n");
					exit(1);
				}
				for (int k=0; k<nd; ++k)
				{
					if (optind>=argc)
					{
						fprintf(stderr,"Missing data argument to command %s\n",c);
						exit(1);
					}
					msg.data[k]=atoi(argv[optind++]);
				}
				msg.dataLen = nd;
				lst.push_back(msg);
			}
}

void SendDiseqcMessages(AudioBuffer & buffer,DiseqcMessageList & lst)
{
	for (DiseqcMessageList::iterator it=lst.begin(); it!=lst.end(); ++it)
	{
		DiseqcMessage & msg = *it;
		buffer.emitMessage(msg.ptr(), msg.numBytes());
		buffer.silence(buffer.msToTicks(1000));
	}
}

static int help(const char * name)
{
	printf("Usage:  [options] commands \n\
Options: \n\
    -a amplitude  (percentage)\n\
    -d device 'hw:2,0' \n\
       Sets the Alsa PCM device name to use \n\
       Use aplay -l to list devices\n\
    -f frequency default 22000 Hz\n\
       Sets the DiSEqC carrier frequency. The standard frequency is 22Khz, but receivers are tolerant. \n\
       If the sound device is unable to reliably generate  the signal, try lower. For example, set \n\
        -f 16000 on  a 48Khz sampling rate device. \n\
    -r rate [Hz]\n\
       sampling rate to be used by the soundcard. Should be multiple of -f \n\
\n\
Commands are specified as a single letter followed by zero up to two arguments: \n\
The arguments are command-specific numbers (bytes). \n\
    G x     GOTO   (6B) \n\
    g x y   GOTO_A (6E) \n\
    h       HALT \n\
    r       RESET \n\
    e x     DRIVE EAST x        \n\
    w x     DRIVE WEST x        \n\
\n\
    The drive commands argument, x, means: \n\
    0       drive continuously \n\
    positive 01 .. 0x7f drive in seconds \n\
    negative 0x80 .. 0xff number of steps \n\
");
	return 0;
}

int main(int argc, char **argv) {
	bool continuousTone = false;
	int rc;
	snd_pcm_t *handle;
	snd_pcm_hw_params_t *params;
	int dir;
	unsigned int channels = 1;
	double rate = 0;	// sampling rate (read back from device after setup)
	std::string deviceName="default";
	int opt;
	double amp=1;
	while ((opt = getopt(argc, argv, "ha:d:f:r:t")) != -1) {
		switch (opt) {
		case 'a':
			amp = atoi(optarg)/100.0;
			break;
		case 'd':
			deviceName=optarg;
			break;
		case 'f':
			AudioBuffer::diseqc_freq = atoi(optarg);
			break;
		case 'h':
			return help(argv[0]);
		case 'r':
			rate = atoi(optarg);
			break;
		case 't':
			continuousTone = true;
			break;
		default: /* '?' */
            help(argv[0]);
			exit(1);
		}
	}

	// parse message commands
	DiseqcMessageList lst;
	BuildDiseqcMessageList(argc, argv, optind, lst);

	/* Open PCM device for playback. */
    printf("Using device '%s'\n",deviceName.c_str());
	rc = snd_pcm_open(&handle, deviceName.c_str(), SND_PCM_STREAM_PLAYBACK, 0);
	if (rc < 0) {
		fprintf(stderr, "unable to open pcm device %s: %s\n", deviceName.c_str(), snd_strerror(rc));
		exit(1);
	}

	/* Allocate a hardware parameters object. */
	snd_pcm_hw_params_alloca(&params);

	/* Fill it in with default values. */
	snd_pcm_hw_params_any(handle, params);

	/* Set the desired hardware parameters. */

	/* Interleaved mode */
	snd_pcm_hw_params_set_access(handle, params, SND_PCM_ACCESS_RW_INTERLEAVED);

	/* Signed 16-bit little-endian format */
	snd_pcm_hw_params_set_format(handle, params, SND_PCM_FORMAT_S16_LE);

	/* set num channels, take in account min/max channels supported */
	unsigned int minch=1, maxch=2;
	snd_pcm_hw_params_get_channels_min(params,&minch);
	snd_pcm_hw_params_get_channels_min(params,&maxch);
	if (channels<minch)
	{
		channels=minch;
		printf("Number of channels adjusted to minimum supported: %u\n",channels);
	}
	if (channels > maxch)
	{
		channels = maxch;
		printf("Number of channels adjusted to maximum supported: %u\n",channels);
	}

	snd_pcm_hw_params_set_channels(handle, params, channels);

	// set sample rate
	unsigned int val;
	if (rate<1)
	{
		// infer from diseqc freq
		rate = AudioBuffer::diseqc_freq * 4;
	}
	val = (unsigned int)rate; // from command line	
	dir = 0;
	snd_pcm_hw_params_set_rate_near(handle, params, &val, &dir);
    printf("Using carrier frequency %g Hz\n", (double)(AudioBuffer::diseqc_freq));
	printf("snd_pcm_hw_params_set_rate_near tried %g got %lu\n", rate, (unsigned long)val);

	// set frame size
	unsigned long frames = AudioBuffer::recommendedFrame(val);
	unsigned long frames1=frames;
	snd_pcm_hw_params_set_period_size_near(handle, params, &frames, &dir);
	printf("snd_pcm_hw_params_set_period_size_near requested %lu got %lu\n",frames1, frames);

	/* Write the parameters to the driver */
	rc = snd_pcm_hw_params(handle, params);
	if (rc < 0) {
		fprintf(stderr, "unable to set hw parameters: %s\n", snd_strerror(rc));
		exit(1);
	}

	AudioBuffer buffer(handle, params);

	buffer.amp=amp;
	printf("Setting amplitude to %g\n",buffer.amp);
	double AngularSpeedEarthInertial=7.2921150E-5*180.0/M_PI;

	printf("Earth's angular speed in inertial space=%g deg/sec\n",AngularSpeedEarthInertial);
	fflush(stdout);
	if (continuousTone) {
		buffer.tone(AudioBuffer::diseqc_freq);
	} else {
		buffer.tone(AudioBuffer::diseqc_freq);
		buffer.pause();
		SendDiseqcMessages(buffer,lst);
	}
	buffer.flush();


	return 0;
}

/* syntax
 * Diseq operation is specified by a sequence of opcodes.
 *
 * An opcode can be one of:
 *   - A byte specified as a hexadecimal number is sent
 *   - A named opcode (this is translated to its byte value)
 *
 *   - special commands
 *         t{ms} = send 22Khz tone for ms. if not specified ms=15
 *         p{ms} = pause (silence) for ms. if not specified ms=15
 *
 */
