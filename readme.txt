Raspberry Pi
============

You might need to set the audio output
https://www.raspberrypi.org/documentation/configuration/audio-config.md

amixer cset numid=3 2

Here the output is being set to 2, which is HDMI.

Setting the output to 1 switches to analogue (headphone jack).

The default setting is 0 which is automatic.
